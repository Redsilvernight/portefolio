<?php

namespace App\DataFixtures;

use App\Entity\Experiences;
use App\Entity\Media;
use App\Entity\Projects;
use App\Entity\Skills;
use App\Entity\User;
use App\Repository\SkillsRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AppFixtures extends Fixture
{
    private $_hasheur;
    private $_manager;
    private $_faker;
    private $_parameterBag;

    public function __construct(UserPasswordHasherInterface $hasheur, SkillsRepository $skillsRepository,ParameterBagInterface $parameterBagInterface)
    {

        $this->_hasheur = $hasheur;
        $this->_faker = Factory::create();
        $this->_skillsRepository = $skillsRepository;
        $this->_parameterBag = $parameterBagInterface;

    }

    public function load(ObjectManager $manager): void
    {
        $this->_manager = $manager;
        $admin = new User();
        $media = new Media();

        $media->setImageName("116002307_274003307232981_2771035440919651040_n.jpg")
            ->setUpdatedAt(new \Datetime("now"));

        $admin->setEmail("florent-1@orange.fr")
            ->setRoles(["ROLE_USER", "ROLE_ADMIN"])
            ->setPassword($this->_hasheur->hashPassword($admin, "roule1grosboze"))
            ->setBirthday(new \DateTime("15-01-2001"))
            ->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, scelerisque sed maximus vel, vulputate at quam. Suspendisse sem dolor, aliquam vitae lacinia id, laoreet dictum felis. Donec consectetur bibendum velit sed mollis. Suspendisse hendrerit dui eu elit consequat, quis ornare odio volutpat. Etiam et risus feugiat, congue odio sit amet, ullamcorper elit. Sed maximus ex odio, non eleifend libero sagittis sagittis. Curabitur malesuada purus ut enim pulvinar, vel rutrum tortor sollicitudin. Curabitur accumsan nunc et malesuada vehicula. Donec aliquam nec lacus id dictum. Nullam eu tellus urna. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.")
            ->setFirstName("Florent")
            ->setLastName("Sabio")
            ->setProfilImage($media)
            ->setAbout('Dévellopeur web Back-end');

        $this->_manager->persist($admin);

        $this->loadSkills();
        $this->_manager->flush();

        $this->loadExperiences();
        $this->loadProjects();

        $this->_manager->flush();
    }

    public function loadSkills() {

        for($count=0; $count < mt_rand(10,15); $count++) {

            $newSkills = new Skills();
    
            $newSkills->setName($this->_faker->text(5))
                    ->setColor(sprintf('#%06X', mt_rand(0, 0xFFFFFF)))
                    ->setDescription($this->_faker->paragraphs(10,true))
                    ->setValue(mt_rand(0,100))
                    ->setIcon("<i class='fab fa-html5'></i>");
            
            $this->_manager->persist($newSkills);
        }
    }

    public function loadExperiences() {
        
        for ($dev=0; $dev < mt_rand(10,25); $dev++) {
            $media = new Media();
            $media->setImageName('placeholder.png')
                ->setUpdatedAt(new DateTime('now'));
    
            $this->_manager->persist($media);
            $newExperience = new Experiences();

            $newExperience->setName($this->_faker->text(mt_rand(5,100)))
                        ->setDescription($this->_faker->paragraphs(mt_rand(5,10),true))
                        ->setStartedAt(new DateTimeImmutable('now'))
                        ->setEndAt(new DateTimeImmutable('now'))
                        ->setType(mt_rand(0,1))
                        ->setMedia($media);
            
            $this->_manager->persist($newExperience);

        }
    }

    public function loadProjects() {

        $skills = $this->_skillsRepository->findAll();

        for($project=0; $project<mt_rand(5,10);$project++) {
            
            $newProject = new Projects();
            $newProject->setName($this->_faker->text(mt_rand(5,50)))
                    ->setCreatedAt(new DateTimeImmutable('now'))
                    ->setDescription($this->_faker->paragraph(mt_rand(5,10),true));
            
            for($projectMedia = 0; $projectMedia<mt_rand(2,10);$projectMedia++) {
                $media = new Media();
                ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 6.0)'); 
                $url = 'https://picsum.photos/200/300';
                $name = md5(uniqid()) . ".jpg";
                $file = $this->_parameterBag->get('project_dir') . "/" . $name;
                file_put_contents($file, file_get_contents($url));
    
                $media->setImageName($name)
                    ->setUpdatedAt(new DateTime('now'));
        
                $this->_manager->persist($media);
                $newProject->addMedias($media);
            }

            
            for($addSkill = 0; $addSkill<mt_rand(2,7); $addSkill++) {
                $newProject->addSkill($skills[mt_rand(1,count($skills)-1)]);
            }
    
            $this->_manager->persist($newProject);
        }
    }
}
