<?php

namespace App\Form;

use App\Entity\Projects;
use App\Entity\Skills;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                "label" => "Nom"
            ])
            ->add('createdAt', DateType::class, [
                "label" => "Date",
                'widget' => 'single_text',
                'input'  => 'datetime_immutable'
            ])
            ->add('description', TextareaType::class, [
                "label" => "Description"
            ])
            ->add('skills', EntityType::class, [
                'label' => "Compétences",
                'placeholder' => '--Compétences utilisées--',
                'class' => Skills::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('medias', CollectionType::class, [
                'label' => "Illustrations",
                'entry_type' => MediaType::class,
                'allow_add'=> true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                "mapped" => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Projects::class,
        ]);
    }
}
