<?php

namespace App\Form;

use App\Entity\User;
use App\Form\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName',TextType::class, [
                "label" => "Prenom"
            ])
            ->add('lastName',TextType::class, [
                "label" => "Nom"
            ])
            ->add('description',TextareaType::class, [
                "label" => "Description",
                "attr" => ['rows' => 10]
            ])
            ->add('profilImage', MediaType::class, [
                "label" => "Photo de profil"
            ])

            ->add('about', TextType::class, [
                "label" => "Poste"
            ])
            ->add('cv', FileType::class, [
                "label" => "CV",
                "mapped" => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
