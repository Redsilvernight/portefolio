<?php

namespace App\Form;

use App\Entity\Experiences;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperiencesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                "label" => "Nom"
            ])
            ->add('startedAt', DateType::class, [
                "label" => "Début",
                'widget' => 'single_text',
                'input'  => 'datetime_immutable'
            ])
            ->add('endAt', DateType::class, [
                "label" => "Fin",
                'widget' => 'single_text',
                'input'  => 'datetime_immutable'
            ])
            ->add('description', TextareaType::class, [
                "label" => "Description"
            ])
            ->add('type', CheckboxType::class, [
                "label" => "Dev",
                "required" => false
            ])
            ->add('media', MediaType::class, [
                "label" => "Illustration"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Experiences::class,
        ]);
    }
}
