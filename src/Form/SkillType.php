<?php

namespace App\Form;

use App\Entity\Skills;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class, [
                "label" => "Nom"
            ])
            ->add('color', ColorType::class, [
                "label" => "Couleur"
            ])
            ->add('value',NumberType::class, [
                "label" => "Valeur (%)"
            ])
            ->add('description',TextareaType::class, [
                "label" => "Description",
                "attr" => ['rows' => 5]
            ])
            ->add('icon',TextType::class, [
                "label" => "Icon"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Skills::class,
        ]);
    }
}
