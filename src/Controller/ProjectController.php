<?php

namespace App\Controller;

use App\Entity\Media;
use App\Entity\Projects;
use App\Form\ProjectsType;
use App\Repository\ProjectsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjectController extends AbstractController
{
    public function __construct(EntityManagerInterface $manager, ProjectsRepository $projectsRepository)
    {
        $this->_manager = $manager;
        $this->_repository = $projectsRepository;
    }
    
    /**
     * @Route("/projects", name="app_projects")
     */
    public function project(): Response
    {
        $projects = $this->_repository->findAll();
        return $this->render('project/index.html.twig', [
            'projects' => $projects,
        ]);
    }

    /**
     * @Route("/projects/list", name="admin_listProjects")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function list(Request $request): Response
    {
        $projects = $this->_repository->findAll();
        $newProject = new Projects();

        $formProject = $this->createForm(ProjectsType::class, $newProject);

        $formProject->handleRequest($request);

        if($formProject->isSubmitted()) {
            foreach($formProject->get("medias")->getData() as $media) {
                $newProject->addMedias($media);
            }
            $this->_manager->persist($newProject);
            $this->_manager->flush();
        }
        return $this->render('project/list.html.twig', [
            'formProject' => $formProject->createView(),
            'projects' => $projects
        ]);
    }

    /**
     * @Route("/projects/edit/{id}", name="admin_editProject")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function edit(Projects $project, Request $request)
    {
        $form = $this->createForm(ProjectsType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            foreach ($form->get("medias")->getData() as $media) {
                $project->addMedias($media);
            }
            $this->_manager->flush();

            return $this->redirectToRoute('admin_dashboard', [
                'section' => 4
            ]);
        }

        return $this->render('project/edit.html.twig', [
            'form' => $form->createView(),
            'project' => $project
        ]);
    }

    /**
     * @Route("/projects/remove/{id}", name="admin_removeProjects")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function remove(Projects $project, Request $request)
    {
        $this->_manager->remove($project);
        $this->_manager->flush();

        return $this->redirectToRoute('admin_dashboard', [
            'section' => 4
        ]);
    }

    /**
     * @Route("/projects/{id}", name="app_show_project")
     */
    public function show(Projects $project, Request $request)
    {
        $mediasProject = $project->getMedias();
        $listName = [];

        foreach ($mediasProject as $media) {
            array_push($listName, $media->getImageName());
        }
        return $this->render("project/show.html.twig", [
            "project" => $project,
            "mediasProject" => $listName
        ]);
    }

    /**
     * @Route("/projects/edit/media/{id}", name="app_remove_projectMedia")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function removeMedia(Media $media) {
        $media->getProjects()->removeMedias($media);

        $this->_manager->remove($media);
        
        $this->_manager->flush();

        return $this->redirectToRoute('admin_dashboard', [
            'section' => 4
        ]);
    }
}
