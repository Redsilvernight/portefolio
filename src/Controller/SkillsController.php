<?php

namespace App\Controller;

use App\Entity\Skills;
use App\Form\SkillType;
use App\Repository\SkillsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SkillsController extends AbstractController
{
    /**
     * @Route("/skills", name="app_skills")
     */
    public function skills(SkillsRepository $skillsRepository): Response
    {
        $skills = $skillsRepository->findAll();

        return $this->render('skills/index.html.twig', [
            'skills' => $skills
        ]);
    }

    /**
     * @Route("/skills/list", name="admin_listSkills")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function list(Request $request, EntityManagerInterface $manager, SkillsRepository $skillsRepository): Response
    {
        $skills = $skillsRepository->findAll();
        $skill = new Skills();
        $form = $this->createForm(SkillType::class, $skill);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $manager->persist($skill);
            $manager->flush();
        }

        return $this->render('skills/list.html.twig', [
            'skillForm' => $form->createView(),
            'skills' => $skills
        ]);
    }

    /**
     * @Route("/skills/remove/{id}", name="admin_removeSkill")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function remove(Skills $skill, Request $request, EntityManagerInterface $manager)
    {
        $manager->remove($skill);
        $manager->flush();

        return $this->redirectToRoute('admin_dashboard', [
            'section' => 2
        ]);
    }

    /**
     * @Route("/skills/{id}", name="admin_showSkill")
     */
    public function show(Skills $skill, Request $request)
    {
        return $this->render('skills/show.html.twig', [
            'skill' => $skill
        ]);
    }

    /**
     * @Route("/skills/edit/{id}", name="admin_editSkill")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function edit(Skills $skill, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(SkillType::class, $skill);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $manager->flush();

            return $this->redirectToRoute('admin_dashboard', [
                'section' => 2
            ]);
        }

        return $this->render('skills/edit.html.twig', [
            'form' => $form->createView(),
            'skill' => $skill
        ]);
    }
}
