<?php

namespace App\Controller;

use App\Entity\Experiences;
use App\Form\ExperiencesType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ExperiencesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ExperiencesController extends AbstractController
{
    public function __construct(EntityManagerInterface $manager, ExperiencesRepository $expRepository)
    {
        $this->_manager = $manager;
        $this->_repository = $expRepository;
    }
    
    /**
     * @Route("/experiences", name="app_experiences")
     */
    public function experiences(): Response
    {
        $experiences = $this->_repository->findBy(['type' => 1]);

        return $this->render('experiences/index.html.twig', [
            "experiences" => $experiences
        ]);
    }

    /**
     * @Route("/experiences/list", name="admin_listExp")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function list(Request $request): Response
    {
        $exps = $this->_repository->findAll();

        $exp = new Experiences();
    
        $formExp = $this->createForm(ExperiencesType::class, $exp);
    
        $formExp->handleRequest($request);
    
        if($formExp->isSubmitted()) {
            $this->_manager->persist($exp);
            $this->_manager->flush();
        }

        return $this->render('experiences/list.html.twig', [
            'formExp' => $formExp->createView(),
            'exps' => $exps
        ]);
    }

    /**
     * @Route("/experiences/edit/{id}", name="admin_editExp")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function edit(Experiences $experience, Request $request)
    {
        $form = $this->createForm(ExperiencesType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->_manager->flush();

            return $this->redirectToRoute('admin_dashboard', [
                'section' => 3
            ]);
        }

        return $this->render('experiences/edit.html.twig', [
            'form' => $form->createView(),
            'experience' => $experience
        ]);
    }

    /**
     * @Route("/experiences/remove/{id}", name="admin_removeExperiences")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function remove(Experiences $experience, Request $request)
    {
        $this->_manager->remove($experience);
        $this->_manager->flush();

        return $this->redirectToRoute('admin_dashboard', [
            'section' => 3
        ]);
    }

    /**
     * @Route("/experiences/type/{type}", name="admin_typeExperiences")
     */
    public function type(Request $request, $type)
    {
        $experiences = $this->_repository->findBy(['type' => !$type]);

        return $this->render('experiences/_timeline.html.twig', [
            "experiences" => $experiences
        ]);
    }

    /**
     * @Route("/experiences/show/{id}", name="app_showExperience")
     */
    public function show(Experiences $experience)
    {
        return $this->render('experiences/show.html.twig', [
            "experience" => $experience
        ]);
    }
}
