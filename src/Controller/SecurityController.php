<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_logout');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/dashboard/{section}", name="admin_dashboard")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function adminDashboard(string $section = '/about/list')
    {
        switch($section) {
            case 2:
                $section = "/skills/list";
                break;
            case 3:
                $section = "/experiences/list";
                break;
            case 4:
                $section = "/projects/list";
                break;

        }

        return $this->render('Admin/dashboard.html.twig', [
            "section" => $section
        ]);
    }
}
