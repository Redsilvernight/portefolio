<?php

namespace App\Controller;

use App\Form\UserType;
use App\Repository\UserRepository;
use App\Repository\SkillsRepository;
use App\Repository\ProjectsRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ExperiencesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    private $user;

    public function __construct(UserRepository $userRepository){
        $this->user = $userRepository->findOneBy(["lastName" => "Sabio"]);
    }

    /**
     * @Route("/", name="app_home")
     */
    public function index(SkillsRepository $skillsRepository, ExperiencesRepository $experiencesRepository, ProjectsRepository $projectsRepository): Response
    {
        $skills = $skillsRepository->findAll();
        $experiences = $experiencesRepository->findBy(['type' => 1]);
        $projects = $projectsRepository->findAll();

        return $this->render('home/index.html.twig', [
            'admin' => $this->user,
            'skills' => $skills,
            'experiences' => $experiences,
            'projects' => $projects
        ]);
    }

    /**
     * @Route("/about/list", name="admin_about")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function edit(Request $request, EntityManagerInterface $manager): Response
    {
        $form = $this->createForm(UserType::class, $this->user);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $cv = $form->get('cv')->getData();
            if($cv != null) {
                $file = md5(uniqid()) . '.' . $cv->guessExtension();
                $cv->move($this->getParameter('images_directory'), $file);
                $this->user->setCvPath($file);
            }
            $manager->flush();
        }

        return $this->render('home/edit.html.twig', [
            'aboutForm' => $form->createView()
        ]);
    }
}
