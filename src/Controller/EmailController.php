<?php

namespace App\Controller;

use App\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    #[Route('/email', name: 'app_email')]
    public function email(Request $request, MailerInterface $mailer): Response
    {
        $formEmail = $this->createForm(MailType::class);

        $formEmail->handleRequest($request);

        if($formEmail->isSubmitted()) {
            $email = (new Email())
                ->from($formEmail->get('from')->getData())
                ->to('redsilvernight@gmail.com')
                ->subject($formEmail->get('subject')->getData())
                ->html("<p>".$formEmail->get('text')->getData()."</p></br><p>Email envoyé de ".$formEmail->get('from')->getData()."</p>")
            ;
            $mailer->send($email);

            return $this->redirectToRoute('app_home');
        }
        return $this->render('email/index.html.twig', [
            'form' => $formEmail->createView()
        ]);
    }
}
