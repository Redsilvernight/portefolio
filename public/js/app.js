var profilImage = document.querySelector('#profilImage');
var loginClose = document.querySelector('#loginClose');
var divLogin = document.querySelector('.login__div');

loginClose.addEventListener('click', function() {
    divLogin.style.display = 'none';
});

profilImage.addEventListener('click', function() {
    var url = "/login";
    var divLoginForm = document.querySelector('#loginDiv');

    axios.get(url).then(function(response) {
        divLoginForm.innerHTML = response.data;
        divLogin.style.display = 'block';
    })
});

loadSelect();
loadTimeline();
loadShow();
loadProjectsPreview();
loadContactMe();

function loadSelect() 
{
    var select = document.querySelector('#selectExps');
    var section = document.querySelector('#divTimeline');
    var sectionDescription = document.querySelector('#expDescription');

    select.onchange = function() {
        var index = this.selectedIndex;
        var url = "/experiences/type/" + index;

        axios.get(url).then(function(response) {
            section.innerHTML = response.data;
            
            loadTimeline();
            loadShow();
            var timelineNext = document.querySelector(".timeline-nav-button--next");
            console.log(timelineNext);
            var showId = document.getElementsByClassName('timeline__item')[0].dataset.id;

            var showUrl = "/experiences/show/" + showId;

            axios.get(showUrl).then(function(response) {
                sectionDescription.innerHTML = response.data;
            })
        })
    }
}

function loadTimeline()
{
    timeline(document.querySelectorAll('.timeline'), {
        forceVerticalMode: 800,
        mode: 'horizontal',
        visibleItems: 4
    });
}

function loadShow()
{
    var itemTimeline = document.getElementsByClassName("timeline__content__wrap");
    var section = document.querySelector('#expDescription');
    for (var i = 0; i < itemTimeline.length; i++) {
        itemTimeline[i].addEventListener('click', function() {
            var item = this;
            var expId = this.children[0].dataset.id;
            var url = "/experiences/show/" + expId;

            axios.get(url).then(function(response) {
                section.innerHTML = response.data;
                var actives = document.querySelectorAll(".timeline__item--active");
                actives.forEach(active => {
                    active.classList.remove("timeline__item--active");
                })
                item.classList.add("timeline__item--active");
            })
        })
    }
}

function loadProjectsPreview() 
{
    var cards = document.getElementsByClassName('card');
    var section = document.getElementById('showProjects');
    var btnClose = document.getElementById('closeShowProjects');
    var sectionShow = document.getElementById('sectionShow');

    for (var c = 0; c < cards.length; c++) {
        cards[c].addEventListener('mouseover', function() {
            var sectionImg = document.getElementById('projectImage_' + this.dataset.id);
            var previewImg = document.getElementById('projectPreview_' + this.dataset.id);
            setTimeout(function() {
                sectionImg.style.display = "none";
                previewImg.style.display = "block";
            },800);
        })
    }

    for (var c = 0; c < cards.length; c++) {
        cards[c].addEventListener('mouseout', function() {
            var sectionImg = document.getElementById('projectImage_' + this.dataset.id);
            var previewImg = document.getElementById('projectPreview_' + this.dataset.id);
            setTimeout(function() {
                sectionImg.style.display = "block";
                previewImg.style.display = "none";
            },800);
        })
    }

    for (var c = 0; c < cards.length; c++) {
        cards[c].addEventListener('click', function() {
            section.style.display = "block";
            var url = "projects/" + this.dataset.id;

            axios.get(url).then(function(response) {
                sectionShow.innerHTML = response.data;
                loadGalleryProject()
            })
                
            btnClose.addEventListener('click', function() {
                section.style.display = "none";
            })
        })
    }
}

function loadGalleryProject() {
    var btnCarousel = document.getElementsByClassName('actionCarousel');
    var img = document.getElementById("imgCarousel");

    var counter = parseInt(document.getElementById("div--carousel").dataset.counter);
    var now = parseInt(document.getElementById("div--carousel").dataset.now);
    var listMedias = JSON.parse(document.getElementById("div--carousel").dataset.medias);

    for (var b = 0; b < btnCarousel.length; b++) {
        btnCarousel[b].addEventListener('click', function() {
            if (this.dataset.action == "next") {
                now = loadNextMedia(now,counter);
            }
            else {
                now = loadPreviousMedia(now,counter);
            }

            var src = "/medias/profil/" + listMedias[now];
            img.src = src;

            document.getElementById("div--carousel").dataset.now = now;
        })
    }
}

function loadNextMedia(now, counter) {
    if (now < counter-1) {
        now += 1;
    } else {
        now = 0
    }

    return now;
}

function loadPreviousMedia(now, counter) {
    if (now > 0) {
        now -= 1;
    } else {
        now = counter-1;
    }

    return now;
}

function loadContactMe() {
    var button = document.getElementById('btnContact');
    var btnClose = document.querySelector('.modal__div__close');
    var url = "/email";
    var section = document.getElementById('modal__sendMail');
    var sectionMail = document.getElementById('formMail');

    button.addEventListener('click', function() {
        axios.get(url).then(function(response) {
            sectionMail.innerHTML = response.data;
            section.style.display = "block";
            loadFormContact();

            btnClose.addEventListener('click', function() {
                section.style.display = "none";
            })
        })
    })
}

function loadFormContact() {
    var formContact = document.getElementById('contactForm');
    var urlAction = formContact.getAttribute("action");

    formContact.addEventListener('submit', function(event) {
        event.preventDefault();
        const formData = new FormData(formContact);

        axios.post(urlAction, formData).then(function() {
            document.location.href = "/";

        });
    })
}