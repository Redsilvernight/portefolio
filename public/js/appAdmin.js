var elements = document.getElementsByClassName("nav__input");
var section = document.getElementById('divDashboard');

var aboutUrl = "/about/list";

for (var i = 0; i < elements.length; i++) {
    elements[i].addEventListener('click', function() {
        var url = this.dataset.url;
        var actives = document.querySelectorAll(".nav__input--active");

        actives.forEach(active => {
            active.classList.remove("nav__input--active");
        })

        this.classList.add("nav__input--active");

        loadSectionContent(url, section);
    });
}



function loadAboutForm() {
    var formAbout = document.getElementById('aboutForm');
    var urlAction = formAbout.getAttribute("action");

    formAbout.addEventListener('submit', function(event) {
        event.preventDefault();
        const formData = new FormData(formAbout);

        axios.post(urlAction, formData).then(function() {
            document.location.href = "/dashboard";

        });
    })
}

function loadEdit(page)
{
    var edit = document.getElementsByClassName("edit");
    var section = document.querySelector(".formDiv");

    for (var e = 0; e < edit.length; e++) {
        edit[e].addEventListener('click', function() {
            var editId = this.id;
            var id = editId.split("_")[2];
            var url = '/' + page[0] + "/edit/" + id;

            axios.get(url).then(function(response) {
                section.innerHTML = response.data;

                if (page[0] == "projects") {
                    loadEditMedias();
                    loadSymfonyCollection();
                }

                var button = document.querySelector(".cancelEdit");

                button.addEventListener('click', function() {
                    document.location.href = "/dashboard/"+page[1];
                })
            })
        })
    }
}

function loadDelete(page) {
    var deleteEntity = document.getElementsByClassName("delete");
    var modal = document.querySelector(".modal");
    var spanDelete = document.querySelector(".spanDelete");
    var deleteClose = document.getElementsByClassName('deleteModalClose');

    for (var d = 0; d < deleteEntity.length; d++) {
        deleteEntity[d].addEventListener('click', function() {
            switch(page) {
                case "skills":
                    var name = "la compétence ";
                    break;
                case "experiences":
                    var name = "l'expérience ";
                    break;
                case "projects":
                    var name = "le projet ";
                    break;
            }
            name = name + this.dataset.name;
            var id = this.dataset.id;
            spanDelete.innerHTML = name;
            modal.style.display = "block";

            var urlDelete = "/" + page + "/remove/"+ id;
            document.querySelector(".linkDelete").href = urlDelete;
        })
    }

    for (var c = 0; c < deleteClose.length; c++) {
        deleteClose[c].addEventListener('click', function(event) {
            modal.style.display = 'none';
        });
    }
}

function loadForm(idForm) 
{
    var form = document.getElementById(idForm[0]);
    var urlAction = form.getAttribute("action");
    var page = [idForm[0].split("_")[0], idForm[1]];

    loadEdit(page);
    loadDelete(page[0]);

    form.addEventListener('submit', function(event) {
        event.preventDefault();
        const formData = new FormData(form);

        axios.post(urlAction, formData).then(function() {
            loadSectionContent(urlAction, section);
        })
    })
}

function loadEditMedias() {
    var modal = document.getElementById("modal__delete_mediaProjects");
    var projectMedias = document.getElementsByClassName("mediaRemove");
    
    for (var p = 0; p < projectMedias.length; p++) {
        projectMedias[p].addEventListener('click', function(event) {
            var name = this.dataset.name;
            var id = this.dataset.id;

            var span = document.querySelector("#spanDelete");
            span.innerHTML = name;

            var url = "/projects/edit/media/" + id;
            var removeTag = document.querySelector("#linkDelete");

            removeTag.setAttribute("href", url);

            modal.style.display = 'block';
        });
    }

    var deleteClose = document.getElementsByClassName('deleteModalClose');
    for (var c = 0; c < deleteClose.length; c++) {
        deleteClose[c].addEventListener('click', function(event) {
            modal.style.display = 'none';
        });
    }
}