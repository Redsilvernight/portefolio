var btnClosePatch = document.querySelector(".patchNoteClose");
var sectionPatch = document.querySelector(".patchNote");
var body = document.getElementById("body");
var html = document.querySelector("html");
var dropdownOpenButton = document.getElementsByClassName("dropdown__button");



btnClosePatch.addEventListener('click', function() {
    sectionPatch.style.display = "none";
    body.classList.remove("body--disabled");
    html.classList.remove("html--disabled");
})

for (var dd = 0; dd < dropdownOpenButton.length; dd++) {
    dropdownOpenButton[dd].addEventListener('click', function() {
        var dropdownId = this.dataset.dropdownid;
        if (this.classList.contains("dropdown__button--close")) {
            document.getElementById(dropdownId).style.display = "none";
            this.classList.remove("fa-caret-up", "dropdown__button--close");
            this.classList.add("fa-caret-down");
        } else {
            document.getElementById(dropdownId).style.display = "block";
            this.classList.remove("fa-caret-down");
            this.classList.add("fa-caret-up", "dropdown__button--close");
        }
    })
}
