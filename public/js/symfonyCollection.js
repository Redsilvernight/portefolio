function loadSymfonyCollection() {
    document.querySelector('.add-field').addEventListener('click', function(event) {
        const list = document.querySelector(this.getAttribute('data-list-selector'));
        var counter = list.getAttribute("data-widget-counter") ||  list.children.length;
        var newWidget = list.getAttribute('data-prototype');
                
        newWidget = newWidget.replace(/__name__/g, counter);
        counter++;
                
        list.setAttribute("data-widget-counter", counter);
                
        var elemId = list.getAttribute('data-widget-tags') + counter;
        var newElem = document.createElement("li");

        newElem.setAttribute("id", elemId);
        newElem.innerHTML = newWidget;

        var button = createRemoveButton(counter);
        newElem.append(button);

        list.appendChild(newElem);
    });
}


function createRemoveButton(counter) {
    var button = document.createElement('button');
    button.setAttribute("type", "button");
    button.setAttribute("onClick", "removeField(this.id)");
    button.setAttribute("id", "btn_" + (counter));

    var icon = document.createElement('i');
    icon.setAttribute("class","far fa-trash-alt ");

    button.append(icon);

    return button;
}

function removeField(id) {
    var liRemove = document.getElementById('liMedia_projet_' + id.split("_")[1]);
    liRemove.remove();
}